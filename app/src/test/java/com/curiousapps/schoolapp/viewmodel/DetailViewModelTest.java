package com.curiousapps.schoolapp.viewmodel;


import androidx.lifecycle.MutableLiveData;

import com.curiousapps.schoolapp.models.SchoolList;
import com.curiousapps.schoolapp.models.SchoolSAT;
import com.curiousapps.schoolapp.repositories.SchoolRepository;
import com.curiousapps.schoolapp.util.InstantExecutorExtension;
import com.curiousapps.schoolapp.util.LiveDataTestUtil;
import com.curiousapps.schoolapp.util.TestUtil;
import com.curiousapps.schoolapp.viewModels.DetailViewModel;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(InstantExecutorExtension.class)
public class DetailViewModelTest extends InstantExecutorExtension{

    private DetailViewModel viewModel;

    @Mock
    private SchoolRepository schoolRepository;

    @BeforeEach
    public void init(){
        MockitoAnnotations.initMocks(this);
        viewModel = new DetailViewModel(schoolRepository);
    }

    @Test
    public void retrieveSingleSchool_returnSchool() throws InterruptedException {
        List<SchoolList> returnedData = TestUtil.TEST_SCHOOL_LIST;
        LiveDataTestUtil<List<SchoolList>> listLiveDataTestUtil = new LiveDataTestUtil<>();
        MutableLiveData<List<SchoolList>> returnedValue = new MutableLiveData<>();
        returnedValue.setValue(returnedData);
        when(schoolRepository.getSchoolList()).thenReturn(returnedValue);

        viewModel.getSchool();
        List<SchoolList> observedData = listLiveDataTestUtil.getValue(viewModel.getSchool());

        assertEquals(returnedData, observedData);
    }
}
