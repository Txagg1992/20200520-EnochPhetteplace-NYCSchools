package com.curiousapps.schoolapp.util;

import com.curiousapps.schoolapp.models.SchoolList;
import com.curiousapps.schoolapp.models.SchoolSAT;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TestUtil {

    public static final List<SchoolList> TEST_SCHOOL_LIST = Collections.unmodifiableList(
            new ArrayList<SchoolList>(){{
                add(new SchoolList("07X547", "New Explorers High School" ));
                add(new SchoolList("21K728", "Liberation Diploma Plus High School" ));
            }}
    );

    public static final List<SchoolSAT> TEST_SCHOOL_SAT = Collections.unmodifiableList(
            new ArrayList<SchoolSAT>(){{
                add(new SchoolSAT("07X547", "New Explorers High School" ));
                add(new SchoolSAT("21K728", "Liberation Diploma Plus High School" ));
            }}
    );
}
