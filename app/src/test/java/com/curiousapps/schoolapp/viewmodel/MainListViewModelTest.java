package com.curiousapps.schoolapp.viewmodel;


import androidx.lifecycle.MutableLiveData;

import com.curiousapps.schoolapp.models.SchoolList;
import com.curiousapps.schoolapp.repositories.SchoolRepository;
import com.curiousapps.schoolapp.util.InstantExecutorExtension;
import com.curiousapps.schoolapp.util.LiveDataTestUtil;
import com.curiousapps.schoolapp.util.TestUtil;
import com.curiousapps.schoolapp.viewModels.MainListViewModel;

//import org.junit.Test;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.Mockito.*;

@ExtendWith(InstantExecutorExtension.class)
public class MainListViewModelTest {
    //system under test
    private MainListViewModel viewModel;

    @Mock
    private SchoolRepository schoolRepository;

    @BeforeEach
    public void init(){
        MockitoAnnotations.initMocks(this);
        viewModel = new MainListViewModel(schoolRepository);
    }

    @Test
    void retrieveSchool_ReturnList() throws InterruptedException {
        //Arrange
        List<SchoolList> returnedData = TestUtil.TEST_SCHOOL_LIST;
        LiveDataTestUtil<List<SchoolList>> liveDataTestUtil = new LiveDataTestUtil<>();
        MutableLiveData<List<SchoolList>> returnedValue = new MutableLiveData<>();
        returnedValue.setValue(returnedData);
        when(schoolRepository.getSchoolList()).thenReturn(returnedValue);
        //Act
        viewModel.getSchoolList();
        List<SchoolList> observedData = liveDataTestUtil.getValue(viewModel.getSchoolList());
        //Assert
        assertEquals(returnedData, observedData);
    }
}
